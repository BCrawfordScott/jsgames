/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/ball.js":
/*!*********************!*\
  !*** ./src/ball.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst defaultAttr ={ pos: { x: 0, y: 0 }, rad: 5 };\n\nclass Ball {\n    constructor(attr) {\n        const attributes = Object.assign({}, defaultAttr, attr);\n\n        const { pos, rad, ctx } = attributes;\n\n        this.pos = pos;\n        this.rad = rad;\n        this.ctx = ctx;\n        this.vector = [2, 4];\n    }\n\n    render() {\n        const { ctx, rad, pos: { x, y } } = this;\n\n        ctx.beginPath();\n        ctx.arc(x, y, rad, 0, Math.PI * 2);\n        ctx.fillStyle = \"#fff\";\n        ctx.fill();\n        ctx.closePath();\n   \n    }\n\n    updatePos(newPos) {\n        if (newPos) {\n            this.pos = newPos;\n            return;\n        }\n        \n        const { pos: { x, y }, vector: [vx, vy]} = this;\n\n        this.pos = { x: x + vx, y: y + vy };\n\n        return;\n    }\n\n    updateVector(delta, reset = false) {\n        if (reset) {\n            this.vector = delta;\n            return;\n        }\n\n        const [vx, vy] = this.vector;\n        const [dx, dy] = delta;\n\n        this.vector = [vx * dx, vy * dy];\n    }\n\n    collision(bounds) {\n        const { xBounds, yBounds } = bounds;\n        const [x, y] = this.pos;\n        const [vx, vy] = this.vector;\n\n        \n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Ball);\n\n//# sourceURL=webpack:///./src/ball.js?");

/***/ }),

/***/ "./src/game.js":
/*!*********************!*\
  !*** ./src/game.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _ball_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ball.js */ \"./src/ball.js\");\n/* harmony import */ var _paddle_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./paddle.js */ \"./src/paddle.js\");\n\n\n\nclass Game {\n    constructor(canvas) {\n        this.ctx = canvas.getContext('2d');\n        this.boardWidth = canvas.width;\n        this.boardHeight = canvas.height;\n\n        this.ball = new _ball_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"]({\n            pos: {x: 6, y: this.boardHeight/2},\n            rad: 5, \n            ctx: this.ctx\n        });\n\n        this.player1Score = 0;\n        this.player2Score = 0;\n        this.player1Paddle = new _paddle_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]({\n            pos: { x: 1, y: this.boardHeight / 2 },\n            ctx: this.ctx,\n        });\n        this.player2Paddle = new _paddle_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]({\n            pos: { x: this.boardWidth - 8, y: this.boardHeight / 2 },\n            ctx: this.ctx,\n        });\n        this.p1Up = false;\n        this.p1Down = false;\n        this.p2Up = false;\n        this.p2Down = false;\n\n        this.winCount = 0;\n\n        this.run = this.run.bind(this);\n        this.keyUpHandler = this.keyUpHandler.bind(this);\n        this.keyDownHandler = this.keyDownHandler.bind(this);\n    }\n\n    render() {\n        this.ctx.clearRect(0, 0, this.boardWidth, this.boardHeight);\n\n        this.renderPaddles();\n        this.ball.render();\n    }\n\n    update() {\n        this.xWallCollision();\n        this.yWallCollision();\n\n        this.updateP1Paddle();\n        this.updateP2Paddle();\n\n        this.ball.updatePos();\n    }\n\n    run() {\n        \n        this.render();\n        this.update();\n        requestAnimationFrame(this.run);\n    }\n\n    play() {\n        this.setListeners();\n\n        requestAnimationFrame(this.run);\n    }\n\n    renderPaddles() {\n        this.player1Paddle.render();\n        this.player2Paddle.render();\n    }\n\n    yWallCollision() {\n        const bounds = [0, this.boardHeight];\n        const pos = this.ball.pos.y + this.ball.vector[1];\n        \n        if (this.wallCollision(bounds, pos)) {\n            this.ball.updateVector([1, -1]);\n        }\n    }\n\n    xWallCollision() {\n        const p1Bounds = 0;\n        const p2Bounds = this.boardWidth;\n\n        const  pos = this.ball.pos.x + this.ball.vector[0];\n        \n        if (pos <= p1Bounds) {\n            this.player2Score = this.player2Score + 1;\n            alert(`Player 2 Scores!\\nScore:\\nPlayer 1: ${this.player1Score} Player 2: ${this.player2Score}`);\n            this.stopPaddles();\n            this.player1Start();\n        }\n\n        if (pos >= p2Bounds) {\n            this.player1Score = this.player1Score + 1;\n            alert(`Player 1 Scores!\\nScore:\\nPlayer 1: ${this.player1Score} Player 2: ${this.player2Score}`);\n            this.stopPaddles();\n            this.player2Start();\n        }\n\n    }\n\n    player2Start() {\n        this.ball.updatePos({ x: this.boardWidth - 5, y: this.boardHeight / 2 });\n        this.ball.updateVector([-2, 4], true);\n    }\n    player1Start() {\n        this.ball.updatePos({ x: 5, y: this.boardHeight / 2 });\n        this.ball.updateVector([2, 4], true);\n    }\n\n    wallCollision(bounds, pos) {\n        const [lb, ub] = bounds;\n        return (pos <= lb || pos >= ub);\n    }\n\n    stopPaddles() {\n        this.p1Up = false;\n        this.p1Down = false;\n        this.p2Up = false;\n        this.p2Down = false;\n    }\n\n    updateP1Paddle() {\n        const { p1Up, p1Down } = this;\n        if (p1Up) {\n            this.updatePaddle(this.player1Paddle, -2);\n        }\n        if (p1Down) {\n            this.updatePaddle(this.player1Paddle, 2);\n        }\n    }\n\n    updateP2Paddle() {\n        const { p2Up, p2Down } = this;\n        if (p2Up) {\n            this.updatePaddle(this.player2Paddle, -2);\n        }\n        if (p2Down) {\n            this.updatePaddle(this.player2Paddle, 2);\n        }\n    }\n\n    updatePaddle(paddle, delta) {\n        paddle.updatePos(delta);\n    }\n\n    setListeners() {\n        document.addEventListener('keydown', this.keyDownHandler);\n        document.addEventListener('keyup', this.keyUpHandler);\n    }\n\n    keyDownHandler(e) {\n        const { key } = e;\n\n        if (key === \"w\") {\n            this.p1Up = true;\n        }\n\n        if (key === \"s\") {\n            this.p1Down = true;\n        }\n\n        if (key === \"Up\" || key === \"ArrowUp\") {\n            this.p2Up = true;\n        }\n\n        if (key === \"Down\" || key === \"ArrowDown\") {\n            this.p2Down = true;\n        }\n    }\n\n    keyUpHandler(e) {\n        const { key } = e;\n\n        if (key === \"w\") {\n            this.p1Up = false;\n        }\n\n        if (key === \"s\") {\n            this.p1Down = false;\n        }\n\n        if (key === \"Up\" || key === \"ArrowUp\") {\n            this.p2Up = false;\n        }\n\n        if (key === \"Down\" || key === \"ArrowDown\") {\n            this.p2Down = false;\n        }\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Game);\n\n//# sourceURL=webpack:///./src/game.js?");

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _game_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./game.js */ \"./src/game.js\");\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ((function() {\n    document.addEventListener('DOMContentLoaded', () => {\n        play();\n    });\n\n    function play() {\n        const canvas = document.getElementById('game-view');\n        canvas.width = 1000;\n        canvas.height = 450;\n\n        const game = new _game_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"](canvas);\n        game.play();\n    }\n})());\n\n//# sourceURL=webpack:///./src/main.js?");

/***/ }),

/***/ "./src/paddle.js":
/*!***********************!*\
  !*** ./src/paddle.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst paddleHeight = 70;\nconst paddleWidth = 7;\n\nclass Paddle {\n  constructor(attr) {\n\n    const { pos: { x, y }, ctx } = attr;\n    this.x = x;\n    this.y = y - paddleHeight / 2;\n    this.ctx = ctx;\n  }\n\n  render() {\n    const { ctx, x, y } = this;\n\n    ctx.beginPath();\n    ctx.rect(x, y, paddleWidth, paddleHeight);\n    ctx.fillStyle = \"white\";\n    ctx.fill();\n    ctx.closePath();\n  }\n\n  updatePos(delta) {\n    this.y = this.y + delta;\n  }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Paddle);\n\n//# sourceURL=webpack:///./src/paddle.js?");

/***/ })

/******/ });