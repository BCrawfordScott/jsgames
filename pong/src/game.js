import Ball from './ball.js';
import Paddle from './paddle.js';

class Game {
    constructor(canvas) {
        this.ctx = canvas.getContext('2d');
        this.boardWidth = canvas.width;
        this.boardHeight = canvas.height;

        this.ball = new Ball({
            pos: {x: 6, y: this.boardHeight/2},
            rad: 5, 
            ctx: this.ctx
        });

        this.player1Score = 0;
        this.player2Score = 0;
        this.player1Paddle = new Paddle({
            pos: { x: 1, y: this.boardHeight / 2 },
            ctx: this.ctx,
        });
        this.player2Paddle = new Paddle({
            pos: { x: this.boardWidth - 8, y: this.boardHeight / 2 },
            ctx: this.ctx,
        });
        this.p1Up = false;
        this.p1Down = false;
        this.p2Up = false;
        this.p2Down = false;

        this.winCount = 0;

        this.run = this.run.bind(this);
        this.keyUpHandler = this.keyUpHandler.bind(this);
        this.keyDownHandler = this.keyDownHandler.bind(this);
    }

    render() {
        this.ctx.clearRect(0, 0, this.boardWidth, this.boardHeight);

        this.renderPaddles();
        this.ball.render();
    }

    update() {
        this.xWallCollision();
        this.yWallCollision();

        this.updateP1Paddle();
        this.updateP2Paddle();

        this.ball.updatePos();
    }

    run() {
        
        this.render();
        this.update();
        requestAnimationFrame(this.run);
    }

    play() {
        this.setListeners();

        requestAnimationFrame(this.run);
    }

    renderPaddles() {
        this.player1Paddle.render();
        this.player2Paddle.render();
    }

    yWallCollision() {
        const bounds = [0, this.boardHeight];
        const pos = this.ball.pos.y + this.ball.vector[1];
        
        if (this.wallCollision(bounds, pos)) {
            this.ball.updateVector([1, -1]);
        }
    }

    xWallCollision() {
        const p1Bounds = 0;
        const p2Bounds = this.boardWidth;

        const  pos = this.ball.pos.x + this.ball.vector[0];
        
        if (pos <= p1Bounds) {
            this.player2Score = this.player2Score + 1;
            alert(`Player 2 Scores!\nScore:\nPlayer 1: ${this.player1Score} Player 2: ${this.player2Score}`);
            this.stopPaddles();
            this.player1Start();
        }

        if (pos >= p2Bounds) {
            this.player1Score = this.player1Score + 1;
            alert(`Player 1 Scores!\nScore:\nPlayer 1: ${this.player1Score} Player 2: ${this.player2Score}`);
            this.stopPaddles();
            this.player2Start();
        }

    }

    player2Start() {
        this.ball.updatePos({ x: this.boardWidth - 5, y: this.boardHeight / 2 });
        this.ball.updateVector([-2, 4], true);
    }
    player1Start() {
        this.ball.updatePos({ x: 5, y: this.boardHeight / 2 });
        this.ball.updateVector([2, 4], true);
    }

    wallCollision(bounds, pos) {
        const [lb, ub] = bounds;
        return (pos <= lb || pos >= ub);
    }

    stopPaddles() {
        this.p1Up = false;
        this.p1Down = false;
        this.p2Up = false;
        this.p2Down = false;
    }

    updateP1Paddle() {
        const { p1Up, p1Down } = this;
        if (p1Up) {
            this.updatePaddle(this.player1Paddle, -2);
        }
        if (p1Down) {
            this.updatePaddle(this.player1Paddle, 2);
        }
    }

    updateP2Paddle() {
        const { p2Up, p2Down } = this;
        if (p2Up) {
            this.updatePaddle(this.player2Paddle, -2);
        }
        if (p2Down) {
            this.updatePaddle(this.player2Paddle, 2);
        }
    }

    updatePaddle(paddle, delta) {
        paddle.updatePos(delta);
    }

    setListeners() {
        document.addEventListener('keydown', this.keyDownHandler);
        document.addEventListener('keyup', this.keyUpHandler);
    }

    keyDownHandler(e) {
        const { key } = e;

        if (key === "w") {
            this.p1Up = true;
        }

        if (key === "s") {
            this.p1Down = true;
        }

        if (key === "Up" || key === "ArrowUp") {
            this.p2Up = true;
        }

        if (key === "Down" || key === "ArrowDown") {
            this.p2Down = true;
        }
    }

    keyUpHandler(e) {
        const { key } = e;

        if (key === "w") {
            this.p1Up = false;
        }

        if (key === "s") {
            this.p1Down = false;
        }

        if (key === "Up" || key === "ArrowUp") {
            this.p2Up = false;
        }

        if (key === "Down" || key === "ArrowDown") {
            this.p2Down = false;
        }
    }
}

export default Game;