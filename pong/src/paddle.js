const paddleHeight = 70;
const paddleWidth = 7;

class Paddle {
  constructor(attr) {

    const { pos: { x, y }, ctx } = attr;
    this.x = x;
    this.y = y - paddleHeight / 2;
    this.ctx = ctx;
  }

  render() {
    const { ctx, x, y } = this;

    ctx.beginPath();
    ctx.rect(x, y, paddleWidth, paddleHeight);
    ctx.fillStyle = "white";
    ctx.fill();
    ctx.closePath();
  }

  updatePos(delta) {
    this.y = this.y + delta;
  }
}

export default Paddle;