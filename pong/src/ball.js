const defaultAttr ={ pos: { x: 0, y: 0 }, rad: 5 };

class Ball {
    constructor(attr) {
        const attributes = Object.assign({}, defaultAttr, attr);

        const { pos, rad, ctx } = attributes;

        this.pos = pos;
        this.rad = rad;
        this.ctx = ctx;
        this.vector = [2, 4];
    }

    render() {
        const { ctx, rad, pos: { x, y } } = this;

        ctx.beginPath();
        ctx.arc(x, y, rad, 0, Math.PI * 2);
        ctx.fillStyle = "#fff";
        ctx.fill();
        ctx.closePath();
   
    }

    updatePos(newPos) {
        if (newPos) {
            this.pos = newPos;
            return;
        }
        
        const { pos: { x, y }, vector: [vx, vy]} = this;

        this.pos = { x: x + vx, y: y + vy };

        return;
    }

    updateVector(delta, reset = false) {
        if (reset) {
            this.vector = delta;
            return;
        }

        const [vx, vy] = this.vector;
        const [dx, dy] = delta;

        this.vector = [vx * dx, vy * dy];
    }

    collision(bounds) {
        const { xBounds, yBounds } = bounds;
        const [x, y] = this.pos;
        const [vx, vy] = this.vector;

        
    }
}

export default Ball;