import Game from './game.js';

export default (function() {
    document.addEventListener('DOMContentLoaded', () => {
        play();
    });

    function play() {
        const canvas = document.getElementById('game-view');
        canvas.width = 1000;
        canvas.height = 450;

        const game = new Game(canvas);
        game.play();
    }
})();